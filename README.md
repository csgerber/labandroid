## Android App Dev UChicago MPCS 51031

This project contains all example projects from the Deitel and Gerber textbooks, as well as other projects for Android 51031. Additional projects may be added throughout the quarter.

##  branches

The master branch is the main branch of this project. In addition, there is an updates branch that may be used to fetch additional projects. 

## Access

You should have at least one remote (gerber) set up for this project that points to git@mit.cs.uchicago.edu:gerber/labAndroid.git (the git protocol may be used if you have set up your secure key)
You may also create your own remote (origin) that points to your own repo. When updates become available, you will fetch them like so: $git fetch gerber updates



